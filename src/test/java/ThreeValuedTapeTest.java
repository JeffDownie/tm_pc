import game.Tape;
import static game.TapeValue.ThreeValuedTape.TapeValueImpl.*;
import junit.framework.TestCase;

public class ThreeValuedTapeTest extends TestCase {

    public void testEmptyTape(){
        Tape emptyTape = new Tape();
        assertEquals(emptyTape.getVal(0), EMPTY);
        assertEquals(emptyTape.getVal(-1), EMPTY);
        assertEquals(emptyTape.getVal(1), EMPTY);
        assertEquals(emptyTape.getVal(1000), EMPTY);
        assertEquals(emptyTape.getVal(-1000), EMPTY);
    }

    public void testFilledTape(){
        Tape filledTape = new Tape();
        filledTape.setVal(3, EMPTY);
        filledTape.setVal(3, ONE);
        filledTape.setVal(-4, ZERO);
        assertEquals(filledTape.getVal(3), ONE);
        assertEquals(filledTape.getVal(-4), ZERO);
        assertEquals(filledTape.getVal(-10), EMPTY);
        assertEquals(filledTape.getVal(10), EMPTY);
    }

    public void testTapeEqualsMethod(){
        Tape tapeOne = new Tape();
        Tape tapeTwo = new Tape();
        assertEquals(tapeOne, tapeTwo);

        tapeOne.setVal(3, EMPTY);
        assertEquals(tapeOne, tapeTwo);

        tapeOne.setVal(3, ONE);
        tapeTwo.setVal(3, ONE);
        tapeOne.setVal(-5, ZERO);
        assertNotSame(tapeOne, tapeTwo);

        tapeTwo.setVal(-5, ZERO);
        assertEquals(tapeOne, tapeTwo);
    }

}