package game;

import game.TapeValue.TapeValue;
import game.TapeValue.ThreeValuedTape.TapeValueFactory;
import javafx.util.Pair;

import java.awt.*;
import java.awt.geom.QuadCurve2D;
import java.util.List;

import static game.Image.ImageUtils.LoadAndSplitImage;

public class Rule implements java.io.Serializable{
	public TapeValue newTapeValue;
	public Direction tapeDirection;
	public State newState;
	private static List<Image> images = null;
	public static Image standardImage = null;
	public static Image selectedImage = null;
	public static Image activeImage = null;
    public static final int RULE_SPHERE_RADIUS = 20;
	
	public Rule(Direction tapeDirection, State newState, TapeValue newTapeValue){
		this.tapeDirection = tapeDirection;
		this.newState = newState;
		this.newTapeValue = newTapeValue;
        if(images == null){
            images = LoadAndSplitImage("/game/images/balls.bmp", 128, 128);
            standardImage = images.get(6).getScaledInstance(RULE_SPHERE_RADIUS*2, RULE_SPHERE_RADIUS*2, 0);
            selectedImage = images.get(0).getScaledInstance(RULE_SPHERE_RADIUS*2, RULE_SPHERE_RADIUS*2, 0);
            activeImage = images.get(1).getScaledInstance(RULE_SPHERE_RADIUS*2, RULE_SPHERE_RADIUS*2, 0);
        }
	}

	public Rule(){
		this.tapeDirection = Direction.LEFT;
		this.newState = State.FINISH_STATE;
		this.newTapeValue = TapeValueFactory.emptyValue();
	}

	public boolean getDirection(){
		return tapeDirection.asBoolean();
	}
	
	State getNewState(){
		return newState;
	}
	
	TapeValue getNewTapeValue(){
		return newTapeValue;
	}
	
	public String toString(){
		return tapeDirection+","+ newTapeValue +","+newState;
	}

	public void draw(Graphics2D graphics2D, int x, int y, State fromState){
        if(newState == null) newState = State.FINISH_STATE; //Fix for circular static build dependency.

        Pair<Integer, Integer> controlPoints = getControlPoints(x, y, fromState);
        graphics2D.draw(new QuadCurve2D.Float(x, y, controlPoints.getKey(), controlPoints.getValue(), newState.x + State.RADIUS, newState.y + State.RADIUS));
	}

    private Pair<Integer, Integer> getControlPoints(int x, int y, State fromState){
        return new Pair<>(x + (x - (fromState.x + State.RADIUS)), y + (y - (fromState.y + State.RADIUS)));
    }
}