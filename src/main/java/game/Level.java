package game;

import game.TapeValue.TapeValue;
import game.TapeValue.ThreeValuedTape.TapeValueFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Level{
	private static Logger logger = Logger.getLogger(Level.class.getName());
	String name;
	public TuringMachine TM;
	public TuringMachine OriginalTM;
	Instructions instructions;
	ArrayList<TuringMachineChecker> CompleteConditions;

	public Rule newRule = new Rule(Direction.LEFT, State.FINISH_STATE, TapeValueFactory.emptyValue());
	public TapeValue oldTapeVal = TapeValueFactory.emptyValue();
	public boolean createRuleToggle = false;
	public State stateForRuleCreation = null;
	public boolean deleteStateToggle = false;
	
	public Level(){
		this.name = "defaultName";
		this.TM = new TuringMachine();
		this.OriginalTM = new TuringMachine();
		this.CompleteConditions = new ArrayList<>();
		this.instructions = new Instructions();
	}
	
	public void test(){
		for(TuringMachineChecker test : CompleteConditions){
			if(!test.testTuringMachine(TM)){
				instructions.useTemporaryText("Not quite... when given " + test.initialConditions.toString() +
						" on the tape, the result should be " + test.expectedResult.toString());
				return;
			}
		}
		instructions.useTemporaryText("Congratulations! Feel free to return to the menu and go to the next level!");
	}

    public static Level simpleLoad(InputStream inputStream){
        Level level = new Level();
        try {
            logger.finer("Parsing input stream.");
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(inputStream));

            logger.finer("Getting name.");
            level.name = (String) jsonObject.get("name");

            logger.finer("Getting tape.");
            JSONArray tape = (JSONArray) jsonObject.get("tape");
            int i = 0;
            for(Object object : tape){
                TapeValue tapeValue = TapeValueFactory.fromString(object.toString());
                level.OriginalTM.tape.setVal(i, tapeValue);
                level.TM.tape.setVal(i, tapeValue);
                i++;
            }

            logger.finer("Getting head position.");
            level.OriginalTM.head = Integer.valueOf(jsonObject.get("head").toString());
            level.TM.head = Integer.valueOf(jsonObject.get("head").toString());

            logger.finer("Getting instructions.");
            JSONArray instructions = (JSONArray) jsonObject.get("instructions");
            for(Object object : instructions){
                level.instructions.appendTutorialText((String) object);
            }

            logger.finer("Getting states.");
            int states = Integer.valueOf(jsonObject.get("states").toString());
            level.TM.addStates(states);
            level.OriginalTM.addStates(states);

            logger.finer("Getting rules.");
            JSONArray rules = (JSONArray) jsonObject.get("rules");
            for(Object object : rules){
                JSONObject rule = (JSONObject) object;
                level.OriginalTM.states.get(rule.get("state").toString()).rules.put(TapeValueFactory.fromString(rule.get("tapeValue").toString()),
                        new Rule(Direction.fromBoolean((boolean)rule.get("direction")), level.OriginalTM.states.get(rule.get("newStateValue").toString()), TapeValueFactory.fromString(rule.get("newTapeValueImpl").toString())));
                level.TM.states.get(rule.get("state").toString()).rules.put(TapeValueFactory.fromString(rule.get("tapeValue").toString()),
                        new Rule(Direction.fromBoolean((boolean) rule.get("direction")), level.TM.states.get(rule.get("newStateValue").toString()), TapeValueFactory.fromString(rule.get("newTapeValueImpl").toString())));
            }

            logger.finer("Getting completeConditions.");
            JSONArray completeConditions = (JSONArray) jsonObject.get("completeConditions");
            for(Object object : completeConditions){
                JSONObject completeCondition = (JSONObject) object;
                JSONArray initialConditions = (JSONArray) completeCondition.get("initialConditions");
                JSONArray expectedResult = (JSONArray) completeCondition.get("expectedResult");
                Tape initialTape = new Tape();
                i = 0;
                for(Object tapeVal : initialConditions){
                    initialTape.setVal(i, TapeValueFactory.fromString(tapeVal.toString()));
                    i++;
                }
                Tape expectedTape = new Tape();
                i = 0;
                for(Object tapeVal : expectedResult){
                    expectedTape.setVal(i, TapeValueFactory.fromString(tapeVal.toString()));
                    i++;
                }
                level.CompleteConditions.add(new TuringMachineChecker(initialTape, expectedTape));
            }

            logger.finer("Getting startingState.");
            level.OriginalTM.startingState = jsonObject.get("startingState").toString();
            level.TM.startingState = jsonObject.get("startingState").toString();

            level.OriginalTM.currentState = level.OriginalTM.startingState;
            level.TM.currentState = level.TM.startingState;

        } catch (Exception e) {
            logger.severe(e.getMessage());
            e.printStackTrace();
        }
        return level;
    }

	public static Level simpleLoad(File file){
        try {
            return simpleLoad(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            logger.severe(e.getMessage());
            return null;
        }
	}

	public Instructions getInstructions() {
		return instructions;
	}


}
