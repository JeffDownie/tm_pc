package game;

import game.Image.ImageUtils;
import game.TapeValue.TapeValue;
import game.TapeValue.ThreeValuedTape.TapeValueFactory;

import java.awt.*;
import java.util.ArrayList;


public class Tape implements java.io.Serializable{
	private ArrayList<TapeValue> PositiveValues;
	private ArrayList<TapeValue> NegativeValues;
    private static Image tapeBackground = null;
    private static Image headBackground = null;
    private static final int TAPE_VISIBLE_SIZE = 40;

	public Tape(){
		PositiveValues = new ArrayList<>();
		NegativeValues = new ArrayList<>();
        if(tapeBackground == null){
            tapeBackground = ImageUtils.loadImage("/game/images/tapeBackgrounds/darkgray.png",
                    TAPE_VISIBLE_SIZE, TAPE_VISIBLE_SIZE);
            headBackground = ImageUtils.loadImage("/game/images/tapeBackgrounds/blue.png",
                    TAPE_VISIBLE_SIZE, TAPE_VISIBLE_SIZE);
        }
	}
	
	private boolean outOfBounds(int position){
		if(position < 0){
			return (-1-position >= NegativeValues.size());
		}else{
			return (position >= PositiveValues.size());
		}
	}
	
	public TapeValue getVal(int position){
		if(outOfBounds(position)){
			return TapeValueFactory.emptyValue();
		}
		
		if(position >= 0){
			return PositiveValues.get(position);
		}else{
			return NegativeValues.get(-1-position);
		}
	}
	
	public void setVal(int position, TapeValue tapeValue){
		if(position >= 0){
			if(position < PositiveValues.size()){
				PositiveValues.set(position, tapeValue);
			}else{
				for(int i = PositiveValues.size(); i < position; i++){
					PositiveValues.add(TapeValueFactory.emptyValue());
				}
				PositiveValues.add(tapeValue);
			}
			
		}else{
			if(-1-position < NegativeValues.size()){
				NegativeValues.set(-1-position, tapeValue);
			}else{
				for(int i = NegativeValues.size(); i < -1-position; i++){
					NegativeValues.add(TapeValueFactory.emptyValue());
				}
				NegativeValues.add(tapeValue);
				
			}
		}
	}
	
	private int firstNonZeroIndex(){
		//returns 0 if all zeros.
		for(int i = -1-this.NegativeValues.size(); i < this.PositiveValues.size(); i++){
			if(this.getVal(i) != TapeValueFactory.emptyValue()){
				return i;
			}
		}
		return 0;
	}
	
	private int lastNonZeroIndex(){
		//returns 0 if all zeros.
		for(int i = -1+this.PositiveValues.size(); i > -this.NegativeValues.size(); i--){
			if(this.getVal(i) != TapeValueFactory.emptyValue()){
				return i;
			}
		}
		return 0;
	}
	
	public void paint(Graphics2D g2d, Canvas c, int head){
		int numTapeShown = c.getWidth()/40;
        int middleIndex = numTapeShown/2;
		for(int i = 0; i <= numTapeShown; i++){
            g2d.drawImage((i == middleIndex) ? headBackground : tapeBackground, 40 * i, c.getHeight() - 40, null);
			getVal(head + i - numTapeShown/2).draw(g2d, 40 * i + 20, c.getHeight() - 20);
		}
	}

	@Override
	public boolean equals(Object object){
		if(object == null || !(object instanceof Tape)){
			return false;
		}
		Tape otherTape = (Tape) object;

		int thisStartIndex = this.firstNonZeroIndex();
		int otherStartIndex = otherTape.firstNonZeroIndex();
		int thisFinalIndex = this.lastNonZeroIndex();
		int otherFinalIndex = otherTape.lastNonZeroIndex();
		
		int thisLengthNonZeros = thisFinalIndex-thisStartIndex;
		int otherLengthNonZeros = otherFinalIndex-otherStartIndex;
		
		if(thisLengthNonZeros!=otherLengthNonZeros){
			//easy case, difference in size.
			return false;
		}else{
			int i = 0;
			while(i+thisStartIndex <= thisFinalIndex){
				if(this.getVal(i+thisStartIndex) != otherTape.getVal(i+otherStartIndex)){
					return false;
				}
				i++;
			}
			
			return true;
		}
	}

	@Override
	public String toString(){
		String retStr = "";
		for(int i = this.firstNonZeroIndex(); i <= this.lastNonZeroIndex(); i++){
			retStr = retStr + this.getVal(i);
		}

		return retStr;
	}
}
