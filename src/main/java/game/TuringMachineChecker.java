package game;

import game.Screen.LevelScreen;

import java.util.logging.Logger;

public class TuringMachineChecker{
	private static Logger logger = Logger.getLogger(TuringMachineChecker.class.getName());
	Tape initialConditions;
	Tape expectedResult;
	int maxSteps = 1000;
	
	TuringMachineChecker(Tape initialConditions, Tape expectedResult){
		try{
			this.initialConditions = (Tape) LevelScreen.deepCopy(initialConditions);
			this.expectedResult = (Tape) LevelScreen.deepCopy(expectedResult);
		} catch (Exception e) {
			logger.severe(e.getMessage());
		}
	}
	
	public boolean testTuringMachine(TuringMachine tMToTestBase){
		TuringMachine turingMachineToTest = new TuringMachine();

		try{
			turingMachineToTest.tape = (Tape) LevelScreen.deepCopy(initialConditions);
		} catch (Exception e) {
			logger.severe(e.getMessage());
		}
		logger.fine("Starting tape: " + turingMachineToTest.tape);
		turingMachineToTest.states = tMToTestBase.states;
		turingMachineToTest.currentState = tMToTestBase.startingState;

		logger.fine("Current state: " + turingMachineToTest.currentState);
		while(!turingMachineToTest.hasHalted()){
			logger.finer("Stepping test TM forward");
			turingMachineToTest.step();
			if(turingMachineToTest.getNumSteps()>maxSteps){
				return false;
			}
		}
		logger.fine("Ending tape: "+turingMachineToTest.tape);
		logger.fine("Expected tape: "+expectedResult);
		return turingMachineToTest.tape.equals(expectedResult);
	}
}
