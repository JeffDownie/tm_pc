package game.Buttons;

import game.Level;

public class DeleteButton extends HighlightModeButton{
	public DeleteButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, text);
	}
	
	public void onPush(Level level){
		super.onPush(level);
		level.deleteStateToggle = !level.deleteStateToggle;
	}
}
