package game.Buttons;

import game.Level;
import game.TuringMachine;
import game.Screen.LevelScreen;

public class ResetButton extends Button{
	public ResetButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, text);
	}
	
	public void onPush(Level level){
		super.onPush(level);
		try {
			level.TM = (TuringMachine) LevelScreen.deepCopy(level.OriginalTM);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
