package game.Buttons;

import game.Level;

public abstract class HighlightModeButton extends Button{
	private boolean isButtonOn;

	HighlightModeButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, text);
		isButtonOn = false;
	}

	public void onPush(Level level){
		
		isButtonOn = !isButtonOn;
		this.selected = isButtonOn;
	}
	
	public void onRelease(Level level){
		
	}
}
