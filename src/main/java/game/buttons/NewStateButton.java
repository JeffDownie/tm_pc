package game.Buttons;

import game.Level;

public class NewStateButton extends Button{
	public NewStateButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, text);
	}
	
	public void onPush(Level level){
		super.onPush(level);
		level.TM.addState();
	}
}
