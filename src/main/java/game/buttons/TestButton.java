package game.Buttons;

import game.Level;

public class TestButton extends Button{

	public TestButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, text);
	}
	
	public void onPush(Level level){
		super.onPush(level);
		level.test();
	}
}
