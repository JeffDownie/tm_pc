package game.Buttons;

/**
 * Created by Jeff on 22/02/2015.
 */
public abstract class AbstractHighlightButton extends AbstractButton {
    public AbstractHighlightButton(int x, int y, int width, int height, String text) {
        super(x, y, width, height, text);
    }

    @Override
    public void pressed(){
        selected = !selected;
        doAction();
    }
}
