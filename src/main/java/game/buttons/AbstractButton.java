package game.Buttons;

import game.Image.ImageUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RGBImageFilter;
import java.io.IOException;
import java.util.logging.Logger;

public abstract class AbstractButton{
    private static Logger logger = Logger.getLogger(AbstractButton.class.getName());
    int x;
    int y;
    int width;
    int height;
    String text;
    public boolean selected;
    private static Image standardImage = null;
    private static Image pressedImage = null;

    public AbstractButton(int x, int y, int width, int height, String text){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.text = text;
        this.selected = false;
        if(standardImage == null){
                standardImage = ImageUtils.FilterPinkToTransparentImage(
                        ImageUtils.loadImage("/game/images/button.bmp"));
        }
        if(pressedImage == null && standardImage != null){
            pressedImage = ImageUtils.FilterImage(standardImage, new RGBImageFilter() {
                @Override
                public int filterRGB(int x, int y, int rgb) {
                    return (rgb & 0xFFFF0000) | ((rgb & 0x0000FF00) << 8) | ((rgb & 0x000000FF) << 16);
                }
            });
        }
    }

    public void paint(Graphics2D g2d, Canvas c){
        if(!selected){
            g2d.drawImage(standardImage, x, y, width, height, null);
        }else{
            g2d.drawImage(pressedImage, x, y, width, height, null);
        }
        g2d.setColor(Color.white);
        g2d.drawString(text, x+5, y+height/2+2);
        g2d.setColor(Color.black);
    }

    public boolean isPressed(int x, int y){
        return (this.x < x && this.y < y && this.x+this.width > x && this.y + this.height > y);
    }

    public void pressed(){
        selected = true;
        doAction();
    }

    protected abstract void doAction();

    public void released(){
        selected = false;
    }
}