package game.Buttons;

import game.Level;

public class NewRuleButton extends Button{
	public enum newRuleInputs{
		TAPE_VALUE, TAPE_DIRECTION, NEW_TAPE_VALUE, CREATE_RULE_TOGGLE
	}
	
	public newRuleInputs nRI;

	public NewRuleButton(int x, int y, int width, int height, String text, newRuleInputs nri) {
		super(x, y, width, height, text);
		nRI = nri;
	}
	
	public void onPush(Level level){
		super.onPush(level);
		switch(nRI){
		case TAPE_DIRECTION:
			level.newRule.tapeDirection = level.newRule.tapeDirection.nextValue();
			this.text = level.newRule.tapeDirection.toString();
			break;
		case TAPE_VALUE:
			level.oldTapeVal = level.oldTapeVal.nextValue();
			this.text = level.oldTapeVal.toString();
			break;
		case NEW_TAPE_VALUE:
			level.newRule.newTapeValueImpl = level.newRule.newTapeValueImpl.nextValue();
			this.text = level.newRule.newTapeValueImpl.toString();
			break;
		case CREATE_RULE_TOGGLE:
			if(level.createRuleToggle){
				//ensure we forget previous state if we are turning it off.
				level.stateForRuleCreation = null;
			}
			level.createRuleToggle = !level.createRuleToggle;
			break;
		}
	}
}
