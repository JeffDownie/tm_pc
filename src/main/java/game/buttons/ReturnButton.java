package game.Buttons;

import game.Level;
import game.Screen.MenuScreen;
import game.MainFrame;

public class ReturnButton extends HighlightModeButton{
	public ReturnButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, text);
	}
	
	public void onPush(Level level){
		super.onPush(level);
		MainFrame.switchScreen(new MenuScreen());
	}
}
