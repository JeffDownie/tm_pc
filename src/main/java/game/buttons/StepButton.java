package game.Buttons;

import game.Level;

public class StepButton extends Button{
	public StepButton(int x, int y, int width, int height, String text) {
		super(x, y, width, height, text);
	}
	
	public void onPush(Level level){
		super.onPush(level);
		level.TM.step();
	}
}