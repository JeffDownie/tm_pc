package game.Image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.RGBImageFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Utility class for image processing.
 * Created by Jeff on 21/02/2015.
 */
public class ImageUtils {
    private static final Logger logger = Logger.getLogger(ImageUtils.class.getName());

    public static Image FilterPinkToTransparentImage(Image originalImage){
        ImageFilter imageFilter = new RGBImageFilter() {
            int markerColour = -65281;
            @Override
            public int filterRGB(int x, int y, int rgb) {
                if(rgb == markerColour) return 0x00000000;
                return rgb;
            }
        };
        return FilterImage(originalImage, imageFilter);
    }

    public static Image FilterImage(Image originalImage, ImageFilter imageFilter){
        return Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(originalImage.getSource(), imageFilter));
    }

    public static List<Image> LoadAndSplitImage(String resourceLocation, int imageWidths, int imageHeights){
        Image completeImage;
        completeImage = ImageUtils.FilterPinkToTransparentImage(
                loadImage(resourceLocation));
        return splitImage(completeImage, imageWidths, imageHeights);
    }

    private static List<Image> splitImage(Image image, int widths, int heights){
        if(image.getHeight(null) % heights != 0 || image.getWidth(null) % widths != 0){
            throw new IllegalArgumentException("Cannot split image into sections that do not evenly divide the image." +
                    "\n Image size: " + image.getWidth(null)+","+image.getHeight(null)+" into sections of size: "+widths+","+heights);
        }

        List<Image> images = new ArrayList<>();
        for(int i = 0; i < image.getWidth(null)/widths; i++){
            for(int j = 0; j < image.getHeight(null)/heights; j++){
                Image cutImage = new BufferedImage(widths, heights, BufferedImage.TYPE_INT_ARGB);
                Graphics graphics = cutImage.getGraphics();
                graphics.drawImage(image, 0, 0, widths, heights, widths*i, heights*j, widths*(i+1), heights*(j+1), null);
                graphics.dispose();
                images.add(cutImage);
            }
        }

        return images;
    }

    public static Image loadImage(String location){
        try {
            return ImageIO.read(ImageUtils.class.getResource(location));
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return null;
        }
    }

    public static Image loadImage(String location, int width, int height){
        Image image = loadImage(location);
        if(image != null){
            return image.getScaledInstance(width, height, 0);
        }
        return null;
    }
}
