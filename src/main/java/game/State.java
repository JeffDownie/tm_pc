package game;

import game.TapeValue.TapeValue;
import game.TapeValue.ThreeValuedTape.TapeValueFactory;
import javafx.util.Pair;

import java.awt.*;
import java.util.HashMap;

import static game.Image.ImageUtils.LoadAndSplitImage;


public class State implements java.io.Serializable{
	public static final int RADIUS = 40;
	public String name;
	public int x;
	public int y;
	public boolean selected;
	public HashMap<TapeValue, Rule> rules;
	public static final State FINISH_STATE = new State("Halt");
	private static java.util.List<Image> images = null;
	private static Image standardImage = null;
	private static Image selectedImage = null;
	private static Image activeImage = null;
	
	State(String name){
		x = (int)(460*Math.random());
		y = 70+(int)(300*Math.random());
		selected = false;
		this.name = name;
        if(images == null){
            images = LoadAndSplitImage("/game/images/balls.bmp", 128, 128);
            standardImage = images.get(7).getScaledInstance(RADIUS*2, RADIUS*2, 0);
            selectedImage = images.get(0).getScaledInstance(RADIUS * 2, RADIUS * 2, 0);
            activeImage = images.get(1).getScaledInstance(RADIUS*2, RADIUS*2, 0);
        }
        rules = new HashMap<>();
        for (TapeValue tapeValue : TapeValueFactory.getAllValues()){
            rules.put(tapeValue, new Rule());
        }
	}
	
	public boolean isPressed(int x, int y){
        for (TapeValue tapeValue : TapeValueFactory.getAllValues()){
            if (isRulePressed(x, y, tapeValue)) return true;
        }
		return (Math.pow((Math.pow((this.x+RADIUS - x),2) + Math.pow((this.y+RADIUS - y),2)),0.5) <= RADIUS);
	}

    public boolean isRulePressed(int x, int y, TapeValue tapeValue){
        int tapeValueX = (int) (this.x + RADIUS + (RADIUS + Rule.RULE_SPHERE_RADIUS) * Math.sin(2 * Math.PI * ((double) TapeValueFactory.getAllValues().indexOf(tapeValue)) / (TapeValueFactory.getAllValues().size())));
        int tapeValueY = (int) (this.y + RADIUS + (RADIUS + Rule.RULE_SPHERE_RADIUS) * Math.cos(2 * Math.PI * ((double) TapeValueFactory.getAllValues().indexOf(tapeValue)) / (TapeValueFactory.getAllValues().size())));
        return (Math.pow((Math.pow((tapeValueX - x),2) + Math.pow((tapeValueY - y),2)),0.5) <= Rule.RULE_SPHERE_RADIUS);
    }
	
	public void paint(Graphics2D g2d, Canvas c, boolean isCurrentState, TapeValue currentTapeValue){
		if(selected){
			g2d.drawImage(selectedImage,x,y,null);
		}else if(isCurrentState) {
			g2d.drawImage(activeImage,x,y,null);
		}else{
			g2d.drawImage(standardImage,x,y, null);
		}

		g2d.setColor(Color.white);
		g2d.drawString(name, this.x + RADIUS - (int)(g2d.getFontMetrics().getStringBounds(name, g2d).getWidth())/2,
				this.y+RADIUS+5);

		int i = 0;
		for(TapeValue tapeValue : TapeValueFactory.getAllValues()){
            Pair<Integer, Integer> tapeValueLocation = getTapeValueCentre(tapeValue);
            int tapeValueX = tapeValueLocation.getKey();
            int tapeValueY = tapeValueLocation.getValue();
			g2d.drawImage(Rule.standardImage, tapeValueX - Rule.RULE_SPHERE_RADIUS, tapeValueY - Rule.RULE_SPHERE_RADIUS, null);
            tapeValue.draw(g2d, tapeValueX, tapeValueY);
			i++;
		}

        g2d.setColor(Color.white);
		for(Rule rule : rules.values()){
			if(!rule.getNewState().equals(FINISH_STATE)){
				g2d.drawLine(this.x+RADIUS, this.y+RADIUS, rule.getNewState().x+RADIUS, rule.getNewState().y+RADIUS);
			}
		}
	}

	public void removeStateDestination(State state){
		rules.keySet().stream().filter(
				tapeValue -> rules.get(tapeValue).newState == state)
				.forEach(rules::remove);
	}

	public Rule getRule(TapeValue tapeValue) {
		if(rules.get(tapeValue) != null){
			return rules.get(tapeValue);
		}
		return new Rule();
	}

    public Pair<Integer, Integer> getTapeValueCentre(TapeValue tapeValue){
        int i = TapeValueFactory.getAllValues().indexOf(tapeValue);
        int tapeValueX = (int) (this.x + RADIUS + (RADIUS + Rule.RULE_SPHERE_RADIUS) * Math.sin(2 * Math.PI * ((double) i) / (TapeValueFactory.getAllValues().size())));
        int tapeValueY = (int) (this.y + RADIUS + (RADIUS + Rule.RULE_SPHERE_RADIUS) * Math.cos(2 * Math.PI * ((double) i) / (TapeValueFactory.getAllValues().size())));
        return new Pair<>(tapeValueX, tapeValueY);
    }

	@Override
	public boolean equals(Object object) {
		return object.getClass() == this.getClass() && ((State) object).name.equals(this.name);
	}
}
