package game;

public enum Direction {
    LEFT, RIGHT;

    @Override
    public String toString() {
        return this == LEFT ? "left" : "right";
    }

    public boolean asBoolean(){
        return this == LEFT;
    }

    public static Direction fromBoolean(boolean bool){
        return bool ? LEFT : RIGHT;
    }

    public Direction nextValue(){
        return this == LEFT ? RIGHT : LEFT;
    }
}
