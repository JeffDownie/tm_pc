package game;

import java.awt.*;
import java.util.ArrayList;


public class Instructions{
	private ArrayList<TutorialText> individualTexts;
	private int currentText;
	TutorialText temporaryText;
	
	Instructions(){
		this.individualTexts = new ArrayList<>();
		//add an empty text to ensure that it works on first paint etc.
		this.individualTexts.add(new TutorialText(""));
		this.currentText = 0;
		this.temporaryText = new TutorialText("");
	}
	
	public void appendTutorialText(String newText){
		//Removes empty txt if it exists.
		if(individualTexts.size() == 1 && individualTexts.get(0).text[0].equals("Click to loop through the instructions.")){
			individualTexts.set(0, new TutorialText(newText));
		}else{
			individualTexts.add(new TutorialText(newText));
		}
	}
	
	public void paint(Graphics2D g2d, Canvas c){
		if(currentText == -1){
			temporaryText.paint(g2d, c);
		}else{
			individualTexts.get(currentText).paint(g2d, c);
		}
	}
	
	public void nextText(){
		if(currentText == -1){
			//Return to start.
			currentText = 0;
		}else{
			//Increments the instruction we a viewing (loops around with the %).
			currentText = (currentText + 1) % individualTexts.size();
		}
	}
	
	public void previousText(){
		System.out.println(currentText);
		if(currentText == -1){
			//Return to start.
			currentText = 0;
		}else{
			//Decrements the instruction we a viewing (loops around with the %).
			currentText = (currentText - 1) % individualTexts.size();
			//Protects from returning to the temporaryText val - modulo chooses the sign of the input.
			if(currentText < 0){
				currentText += individualTexts.size();
			}
		}
		System.out.println(currentText);
	}

	public boolean isPressed(int x, int y) {
		if(currentText == -1){
			//Return to start.
			return temporaryText.isPressed(x, y);
		}else{
			return individualTexts.get(currentText).isPressed(x, y);
		}
	}

	public void useTemporaryText(String string) {
		//Flag for using temp text
		currentText = -1;
		this.temporaryText = new TutorialText(string);
	}
}
