package game;

import game.Screen.MenuScreen;
import game.Screen.Screen;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.*;

public class MainFrame extends Frame {
	private static Logger logger = Logger.getLogger(MainFrame.class.getCanonicalName());
	private static MainFrame frame;
	public MainFrame() {
		super("TMS");
		logger.fine("Creating new MainFrame.");

		Component menuScreen = new MenuScreen();
		this.add(menuScreen);

		this.setSize(1024,768);
		this.setVisible(true);
		addWindowListener(new closeWindowEventHandler());
		this.setBackground(Color.white);
	}

	public static void switchScreen(Screen switchTo){
		frame.removeAll();
		frame.add(switchTo);
		frame.setVisible(true);
		frame.repaint();
	}


	class closeWindowEventHandler extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		frame = new MainFrame();
	}
}