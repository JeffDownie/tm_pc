package game.Screen;

import game.Buttons.*;
import game.Image.ImageUtils;
import game.Level;
import game.MainFrame;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

public class MenuScreen extends Screen {
    private static Logger logger = Logger.getLogger(MenuScreen.class.getName());
    ArrayList<AbstractButton> buttons;

    public MenuScreen(){
        this.addMouseListener(this);
        buttons = new ArrayList<>();
        int numLevels = getNumLevels();
        for (int levelNumber = 1; levelNumber <= numLevels; levelNumber++){
            buttons.add(new AbstractButton(60 * (levelNumber - 1), 0, 60, 50, "Level " + levelNumber) {
                private int innerLevelNumber;

                @Override
                public void doAction() {
                    Level level = Level.simpleLoad(MenuScreen.class.getResourceAsStream(
                            "/levels/level" + innerLevelNumber + ".tmlvl"));
                    LevelScreen levelScreen = new LevelScreen();
                    levelScreen.level = level;
                    MainFrame.switchScreen(levelScreen);
                }

                private AbstractButton init(int innerLevelNumber) {
                    this.innerLevelNumber = innerLevelNumber;
                    return this;
                }
            }.init(levelNumber));
        }
        backgroundImage = ImageUtils.loadImage("/game/images/backgrounds/redplanet.png");
    }

    @Override
    public void draw(Graphics2D graphics2D){
        graphics2D.setColor(Color.black);
        for(AbstractButton b : buttons){
            b.paint(graphics2D, this);
        }
    }

    private static int getNumLevels(){
        Properties properties = new Properties();
        try {
            properties.load(MenuScreen.class.getResourceAsStream("/levels/levels.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return Integer.parseInt(properties.getProperty("numLevels"));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        buttons.stream().filter(b -> b.isPressed(e.getX(), e.getY())).forEach(game.Buttons.AbstractButton::pressed);
        this.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.buttons.stream().filter(b -> b.selected).forEach(game.Buttons.AbstractButton::released);
        this.repaint();
    }
}
