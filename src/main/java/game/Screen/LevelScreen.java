package game.Screen;

import game.Buttons.Button;
import game.Buttons.*;
import game.Level;
import game.Rule;
import game.State;
import game.TapeValue.TapeValue;
import game.TapeValue.ThreeValuedTape.TapeValueFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;


public class LevelScreen extends Screen {
	private static Logger logger = Logger.getLogger(Level.class.getName());
	Level level;
	ArrayList<Button> buttons;

	private int previousMouseX;
	private int previousMouseY;
	
	private int screenX;
	private int screenY;

	public double scale;

	private int mouseGameX;
	private int mouseGameY;
	
	
	
	LevelScreen(){
		buttons = new ArrayList<>();
		level = new Level();
		buttons.add(new StepButton(0,0,60,30,"Step"));
		buttons.add(new ResetButton(60,0,60,30, "Reset"));
		buttons.add(new TestButton(370,0,60,60,"Test"));

		buttons.add(new NewRuleButton(190,0,60,30,"_", NewRuleButton.newRuleInputs.TAPE_VALUE));
		buttons.add(new NewRuleButton(250,0,60,30,"left", NewRuleButton.newRuleInputs.TAPE_DIRECTION));
		buttons.add(new NewRuleButton(310,0,60,30,"_", NewRuleButton.newRuleInputs.NEW_TAPE_VALUE));
		buttons.add(new NewRuleButton(190,30,180,30,"Create Rule Mode", NewRuleButton.newRuleInputs.CREATE_RULE_TOGGLE));
		buttons.add(new ReturnButton(430,0,60,60,"Menu"));
		scale = 1.0;
	}

	@Override
	public void draw(Graphics2D graphics2D){
		graphics2D.setColor(Color.black);
			
		//Draw all static things first. TODO: reverse this.
		for(Button b : buttons){
			b.paint(graphics2D, this);
		}
		level.getInstructions().paint(graphics2D, this);
		level.TM.tape.paint(graphics2D, this, level.TM.head);

		//Translate for drawing the states & rules.
		graphics2D.scale(scale, scale);
		graphics2D.translate(-screenX, -screenY);
		level.TM.paint(graphics2D, this);
	}

    @Override
	public void mousePressed(MouseEvent e) {
		mouseGameX = screenX + (int)(e.getX()/scale);
		mouseGameY = screenY + (int)(e.getY()/scale);

		buttons.stream().filter(b -> b.isPressed(e.getX(), e.getY())).forEach(b -> b.onPush(level));
		
		for(State state : level.TM.states.values()){
			if(state.isPressed(mouseGameX, mouseGameY)){
                boolean ruleStartedFlag = false;
                for(TapeValue tapeValue : TapeValueFactory.getAllValues()){
                    if(state.isRulePressed(mouseGameX, mouseGameY, tapeValue)){
                        if(level.createRuleToggle){
                            level.oldTapeVal = tapeValue;
                            if(level.stateForRuleCreation == null){
                                level.stateForRuleCreation = state;
                            }else{
                                level.TM.states.get(level.stateForRuleCreation.name)
                                        .rules.put(level.oldTapeVal, new Rule(
                                        level.newRule.tapeDirection, state, level.newRule.newTapeValue));
                                level.stateForRuleCreation = null;
                            }
                        }else if(level.deleteStateToggle){
                            level.TM.removeState(state);
                        }else{
                            state.selected = true;
                        }
                        ruleStartedFlag = true;
                    }
                }
                if(ruleStartedFlag) continue;

				if(level.createRuleToggle){
					if(level.stateForRuleCreation == null){
						level.stateForRuleCreation = state;
					}else{
						level.TM.states.get(level.stateForRuleCreation.name)
								.rules.put(level.oldTapeVal, new Rule(
										level.newRule.tapeDirection, state, level.newRule.newTapeValue));
						level.stateForRuleCreation = null;
					}
				}else if(level.deleteStateToggle){
					level.TM.removeState(state);
					break;
				}else{
					state.selected = true;
				}
			}
		}
		if(level.getInstructions().isPressed(e.getX(), e.getY())){
			if(SwingUtilities.isLeftMouseButton(e)){
				level.getInstructions().nextText();
			}else if(SwingUtilities.isRightMouseButton(e)){
				level.getInstructions().previousText();
			}
		}
		
		previousMouseX = mouseGameX;
		previousMouseY = mouseGameY;

		this.repaint();
	}

	public void mouseReleased(MouseEvent e) {
		mouseGameX = screenX + (int)(e.getX()/scale);
		mouseGameY = screenY + (int)(e.getY()/scale);

		level.TM.states.values().stream().filter(s -> s.selected).forEach(s -> {
			s.x += mouseGameX - previousMouseX;
			s.y += mouseGameY - previousMouseY;
			s.selected = false;
		});

		this.buttons.stream().filter(b -> b.selected).forEach(b -> {
			if (b.getClass() == NewRuleButton.class) {
				if (((NewRuleButton) b).nRI == NewRuleButton.newRuleInputs.CREATE_RULE_TOGGLE) {
					if (!level.createRuleToggle) {
						b.selected = false;
					}
				} else {
					b.selected = false;
				}
			} else {
				b.onRelease(level);
			}
		});
		
		this.repaint();
		logger.finest("mouse released");
	}
	
	public void mouseDragged(MouseEvent e) {
		mouseGameX = screenX + (int)(e.getX()/scale);
		mouseGameY = screenY + (int)(e.getY()/scale);


		boolean isAStateSelected = false;
		for(State s : level.TM.states.values()){
			if(s.selected){
				s.x += mouseGameX-previousMouseX;
				s.y += mouseGameY-previousMouseY;
				isAStateSelected = true;
			}
		}
		if(!isAStateSelected){
			//no state being move --> Dragging the screen.
			screenX += previousMouseX - mouseGameX;
			screenY += previousMouseY - mouseGameY;
			mouseGameX = screenX + (int)(e.getX()/scale); //fixes flickering.
			mouseGameY = screenY + (int)(e.getY()/scale);
		}

		previousMouseX = mouseGameX;
		previousMouseY = mouseGameY;
		this.repaint();
		logger.finest("mouse dragged");
	}


	public void mouseWheelMoved(MouseWheelEvent e) {
		double oldScale = this.scale;
		mouseGameX = screenX + (int)(e.getX()/scale);
		mouseGameY = screenY + (int)(e.getY()/scale);

		this.scale = Math.max(0.05, Math.min(10.0, scale - ((double)e.getWheelRotation())/20));

		screenX = (int) (mouseGameX - (mouseGameX - screenX)*(oldScale/this.scale));
		screenY = (int) (mouseGameY - (mouseGameY - screenY)*(oldScale/this.scale));

		this.repaint();
	}
	
	//Here for now.
	public static Object deepCopy(Object obj) throws Exception{
		ObjectOutputStream oos = null;
		ObjectInputStream ois = null;
		try
		{
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			// serialize and pass the object
			oos.writeObject(obj);
			oos.flush();
			ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
			ois = new ObjectInputStream(bin);
			return ois.readObject();
		}
		catch(Exception e)
		{
			System.out.println("Exception in deepCopy: " + e);
			throw(e);
		}
		finally
		{
			if (oos != null) {
				oos.close();
			}
			if (ois != null) {
				ois.close();
			}
		}
	}
}