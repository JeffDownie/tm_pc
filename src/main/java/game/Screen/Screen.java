package game.Screen;

import game.Image.ImageUtils;

import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Abstract class for screens to be held in the mainFrame.
 */
public abstract class Screen extends Canvas implements MouseInputListener, MouseWheelListener {
    private static final Logger logger = Logger.getLogger(Screen.class.getName());
    private static final Color backgroundColour = Color.BLACK;
    public static final int NUM_BACKGROUND_IMAGES = 12;
    protected Image backgroundImage = null;

    public Screen(){
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
        int bgNumber = new Random().nextInt(NUM_BACKGROUND_IMAGES - 1) + 1;
        backgroundImage = ImageUtils.loadImage("/game/images/backgrounds/" + bgNumber + ".png");
    }

    @Override
    public final void paint(Graphics graphics) {
        update(graphics);
    }

    @Override
    public final void update(Graphics graphics) {
        BufferedImage bf = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        try {
            Graphics2D graphics2D = (Graphics2D) bf.getGraphics();
            //Clear screen
            graphics2D.setColor(backgroundColour);
            graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());
            graphics2D.drawImage(backgroundImage, 0, 0, null);

            draw(graphics2D);
        } catch (Exception e) {
            logger.severe(e.getMessage());
            e.printStackTrace();
        }
        graphics.drawImage(bf, 0, 0, null);
    }

    protected abstract void draw(Graphics2D graphics2D);

    //Standard do-nothing default implementation.
    @Override
    public void mouseClicked(MouseEvent e) {}
    @Override
    public void mousePressed(MouseEvent e) {}
    @Override
    public void mouseReleased(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}
    @Override
    public void mouseDragged(MouseEvent e) {}
    @Override
    public void mouseMoved(MouseEvent e) {}
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {}
}
