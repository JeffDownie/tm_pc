package game;

import game.Image.ImageUtils;

import java.awt.*;
import java.io.IOException;

public class TutorialText{
	String[] text;
	private int x = 0;
	private int y = 0;
	private int width = 0;
	private int height = 0;
    private static Image background = null;

    TutorialText(String text){
        String[] tempText = stringWrap(text, 30).split("\n");
        this.text = new String[tempText.length+1];
        for(int i = 0; i < tempText.length; i++){
            this.text[i] = tempText[i];
        }
        this.text[tempText.length] = "Click to loop through the instructions.";

        //Wrong, but just so that no error happens
        this.x = 0;
        this.y = 0;
        if(background == null){
            background = ImageUtils.loadImage("/game/images/TextContainers/blue/panel-5.png");
        }
    }
	
	public void paint(Graphics2D g2d, Canvas c){
		int width = 0;
		int height = 0;
		for(String line : this.text){
			width = Math.max((int)(g2d.getFontMetrics().getStringBounds(line, g2d).getWidth()),width);
			height = (int)(g2d.getFontMetrics().getStringBounds(line, g2d).getHeight()) + height;
		}
		
		this.width = width + 50;
		this.height = height + 25;
		this.x = 0;
		this.y = 60;
		
		g2d.drawImage(background, this.x, this.y, this.width, this.height, null);
		for(int i = 0; i < this.text.length; i++){
            g2d.setColor(Color.white);
			g2d.drawString(text[i], this.x + 25,
                    this.y + 15 + i*((int)(g2d.getFontMetrics().getStringBounds(text[i], g2d).getHeight())));
		}
	}

	private static String stringWrap(String s, int charWidth){
		String retStr = "";
		int j = 0;
		for(int i = 0; i < s.length(); i++){
			j++;
			if(j >= charWidth){
				if(s.charAt(i)==' ' || s.charAt(i)=='\n'){
					retStr = retStr + '\n' + s.substring(retStr.length(), i);
					j = 0;
				}
			}
		}
		return retStr + '\n' + s.substring(retStr.length());
	}
	
	public boolean isPressed(int x, int y){
		return (this.x < x && this.y < y && this.x + this.width > x && this.y + this.height > y);
	}
}
