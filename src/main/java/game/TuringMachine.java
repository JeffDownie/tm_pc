package game;

import game.TapeValue.TapeValue;
import game.TapeValue.ThreeValuedTape.TapeValueFactory;
import javafx.util.Pair;

import java.awt.*;
import java.util.HashMap;
import java.util.logging.*;

public class TuringMachine implements java.io.Serializable{
	public Tape tape;
	public int head = 0;
	public String currentState;
	public String startingState;
	public HashMap<String, State> states;
	private int numSteps = 0;
	
	TuringMachine(){
		this.tape = new Tape();
		this.states = new HashMap<>();
		this.head = 0;

		states.put("Halt", State.FINISH_STATE);
		this.currentState = "Halt";
		this.startingState = "Halt";
	}
	
	public void step(){
		Rule applyRule = states.get(currentState)
				.getRule(
						this.tape.getVal(this.head));
		this.tape.setVal(this.head, applyRule.getNewTapeValue());
		this.head = applyRule.getDirection() ? head-1 : head + 1;
		this.currentState = applyRule.getNewState().name;
		numSteps++;
		hasHalted();
	}
	
	public boolean hasHalted(){
		return this.currentState.equals("Halt");
	}
	
	public void addState(){
		states.put(Integer.toString(states.size()), new State(Integer.toString(states.size())));
	}

	public void addStates(int num){
		for(int i = 1; i <= num; i++){
			addState();
		}
	}
	
	public void removeState(State s){
		if(states.size()>1){
			if(s == states.get(currentState)){
				currentState = "Halt";
			}
			for(State state : states.values()){
				state.removeStateDestination(s);
			}
			states.remove(s.name);
		}
	}
	
	public int getNumSteps(){
		return numSteps;
	}
	
	public void paint(Graphics2D g2d, Canvas c){
        for(State state : states.values()){
            for(TapeValue tapeValue : TapeValueFactory.getAllValues()){
                Pair<Integer, Integer> position = state.getTapeValueCentre(tapeValue);
                state.getRule(tapeValue).draw(g2d, position.getKey(), position.getValue(), state);
            }
        }
        for(State state : states.values()){
            state.paint(g2d, c, state.name.equals(currentState), this.tape.getVal(this.head));
        }
	}
}