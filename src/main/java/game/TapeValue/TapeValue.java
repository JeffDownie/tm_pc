package game.TapeValue;

import java.awt.*;
import java.io.Serializable;

public interface TapeValue extends Serializable{
    public TapeValue nextValue();

    public void draw(Graphics2D graphics2D, int centreX, int centreY);

    @Override
    public String toString();
}
