package game.TapeValue.ThreeValuedTape;

import game.Image.ImageUtils;
import game.TapeValue.TapeValue;

import java.awt.*;
import java.util.*;
import java.util.List;

public class TapeValueImpl implements TapeValue {
    private static final int IMAGE_SIZE = 50;
    private static final java.util.List<Image> imageSources = ImageUtils.LoadAndSplitImage("/game/images/sheet_black.bmp", IMAGE_SIZE, IMAGE_SIZE);

    //Would be an enum, but... inheritance.
    public static TapeValue EMPTY = new TapeValueImpl(10);
    public static TapeValue ZERO = new TapeValueImpl(33);
    public static TapeValue ONE = new TapeValueImpl(67);

    static final List<TapeValue> values = Arrays.asList(EMPTY, ZERO, ONE);
    private int imageId;

    //Private constructor to avoid other initializations.
    private TapeValueImpl(int i){
        this.imageId = i;
    }

    @Override
    public String toString() {
        return (this == EMPTY) ? "_" : ((this == ZERO) ? "0" : "1");
    }

    public TapeValue nextValue(){
        return (this == EMPTY) ? ZERO : ((this == ZERO) ? ONE : EMPTY);
    }

    public void draw(Graphics2D graphics2D, int centreX, int centreY){
        graphics2D.drawImage(imageSources.get(imageId), centreX - IMAGE_SIZE/2, centreY - IMAGE_SIZE/2, null);
    }
}
