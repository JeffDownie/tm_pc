package game.TapeValue.ThreeValuedTape;

import game.TapeValue.TapeValue;

import java.util.List;

public class TapeValueFactory {
    public static TapeValue fromString(String value){
        return value.equals("0") ? TapeValueImpl.EMPTY :
                value.equals("1") ? TapeValueImpl.ZERO :
                        TapeValueImpl.ONE;
    };

    public static TapeValue emptyValue(){
        return TapeValueImpl.EMPTY;
    }

    public static List<TapeValue> getAllValues(){
        return TapeValueImpl.values;
    }
}
